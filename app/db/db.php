<?php

//$dbconn = pg_connect("host=postgres dbname=test user=admin password=admin")
//or die(pg_last_error());

try {
    $dbconn = new PDO("pgsql:host=" . getenv('POSTGRES_HOST')
        . ";dbname=" .getenv('POSTGRES_DB')
        . ";user=" . getenv('POSTGRES_USER')
        . ";password=". getenv('POSTGRES_PASSWORD'));
} catch (Exception $e) {
    die("Unable to connect: " . $e->getMessage());
}

//$dbconn = new mysqli("localhost", "sammy", "password",
//    "development_db");
//if ($dbconn->connect_errno) {
//    die("Failed to connect to MySQL: " . $dbconn->connect_error);
//}
