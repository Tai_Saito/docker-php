<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class ThemesMigration extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
        $this->table("themes", ['id' => 'id'])
            ->addColumn('name', 'string')
            ->addColumn('image', 'string', ['null' => true, 'default' => null])
            ->create();
        $this->table("themes")
            ->insert([
            [
                'id' => 1,
                'name' => 'Cinema',
                'image' => 'cinema.jpg'
            ],
            [
                'id' => 2,
                'name' => 'Sport',
                'image' => 'sport.jpg'
            ],
            [
                'id' => 3,
                'name' => 'Music',
                'image' => 'music.jpg'
            ],
            [
                'id' => 4,
                'name' => 'Cooking',
                'image' => 'cooking.jpg'
            ],
            [
                'id' => 5,
                'name' => 'Games',
                'image' => 'games.jpg'
            ],
            [
                'id' => 6,
                'name' => 'Literature',
                'image' => 'literature.jpg'
            ]
        ])->save();
    }
}
