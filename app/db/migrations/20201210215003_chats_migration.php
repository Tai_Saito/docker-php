<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class ChatsMigration extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
        $this->table("chats", ['id' => 'id'])
            ->addColumn('theme_id', 'integer')
            ->addColumn('age_min', 'integer', ['default' => 0])
            ->addColumn('age_max', 'integer', ['null' => true, 'default' => null])
            ->addForeignKey('theme_id', 'themes', 'id',
                ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
            ->create();
    }
}
