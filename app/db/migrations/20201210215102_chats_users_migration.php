<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class ChatsUsersMigration extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
        $this->table("chats_users", ['id' => false, 'primary_key' => ['chat_id', 'users_login']])
            ->addColumn('chat_id', 'integer')
            ->addColumn('users_login', 'string')
            ->addForeignKey('chat_id', 'chats', 'id',
                ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
            ->addForeignKey('users_login', 'users', 'login',
                ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
            ->create();
    }
}
