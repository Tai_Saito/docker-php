<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class LinksMigration extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
        $this->table("links", ['id' => 'link_id'])
            ->addColumn('link', 'string')
            ->addColumn('status', 'string', ['null' => true, 'default' => null])
            ->addColumn('update_time', 'string')
            ->create();
        $this->table("users")
            ->addColumn('link_id', 'integer', ['null' => true, 'default' => null])
            ->addForeignKey('link_id', 'links', 'link_id',
                ['delete'=> 'SET_NULL', 'update'=> 'NO_ACTION'])
            ->update();
    }
}
