<?php

require_once('BaseAPIClass.php');

class Links extends BaseAPIClass {
    public static $class_name = "Links";
    public static $base_route = "/links/";
    public static $http_methods = [
        "getall" => EpiRoute::httpGet,
        "create" => EpiRoute::httpPost
    ];

    // 1 = link exists
    // 2 = link does not exist
    // link => LINKS
    public static function get($link_id) {
        $answer = getAnswerPrototype();

        global $dbconn;
        if (!($query = $dbconn->prepare("SELECT * FROM links WHERE link_id=?"))) {
//        echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
            $answer['message'] = 'Prepare failed';
            $answer['error'] = $dbconn->errorInfo();
            return $answer;
        }

//    if (!$query->bind_param("s", $link)) {
////        echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
//        $answer['message'] = 'Binding parameters failed';
//        $answer['error'] = $query->errorInfo();
//        return $answer;
//    }

        if (!$query->execute([$link_id])) {
//        echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $answer['message'] = 'Execute failed';
            $answer['error'] = $query->errorInfo();
            return $answer;
        }

        $res = $query->fetchAll(PDO::FETCH_ASSOC);
//    if (!($res = $query->get_result())) {
////        echo "Getting result set failed: (" . $stmt->errno . ") " . $stmt->error;
//        $answer['message'] = 'Getting result set failed';
//        $answer['error'] = $query->errorInfo();
//        return $answer;
//    }

        if (count($res) == 0) {
            $answer['status'] = 2;
            $answer['message'] = 'There is no such link';
            return $answer;
        }
//    $res->data_seek(0);

        $answer['status'] = 1;
        $answer['link'] = $res[0];
        return $answer;
    }

    // 1 = all links
    // links => [LINKS]
    public static function getall() {
        $answer = getAnswerPrototype();

        global $dbconn;
        if (!($query = $dbconn->prepare("SELECT * FROM links"))) {
//        echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
            $answer['message'] = 'Prepare failed';
            $answer['error'] = $dbconn->errorInfo();
            return $answer;
        }

        if (!$query->execute()) {
//        echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $answer['message'] = 'Execute failed';
            $answer['error'] = $query->errorInfo();
            return $answer;
        }

        $res = $query->fetchAll(PDO::FETCH_ASSOC);
//    if (!($res = $query->get_result())) {
////        echo "Getting result set failed: (" . $stmt->errno . ") " . $stmt->error;
//        $answer['message'] = 'Getting result set failed';
//        $answer['error'] = $query->errorInfo();
//        return $answer;
//    }

        $answer['status'] = 1;
        $answer['links'] = $res;
        return $answer;
    }

    // link
    // 1 = link created
    // link_id
    public static function create() {
        $arr = Links::getRequestArray('create');
        $answer = getAnswerPrototype();
        if (!isset($arr['link'])) {
            $answer['message'] = 'You must enter link';
            return $answer;
        }
        $link = $arr['link'];

        global $dbconn;
        if (!($query = $dbconn->prepare("INSERT INTO links(link,update_time) VALUES(?,?)"))) {
//        echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
            $answer['message'] = 'Prepare failed';
            $answer['error'] = $dbconn->errorInfo();
            return $answer;
        }

//    if (!$query->bind_param("ss", $link,date('Y/m/d G:i:s'))) {
////        echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
//        $answer['message'] = 'Binding parameters failed';
//        $answer['error'] = $query->errorInfo();
//        return $answer;
//    }

        if (!$query->execute([$link, date('Y/m/d G:i:s')])) {
//        echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $answer['message'] = 'Execute failed';
            $answer['error'] = $query->errorInfo();
            return $answer;
        }

        if (!($query = $dbconn->prepare("SELECT * FROM links WHERE link=? ORDER BY 1 DESC"))) {
//        echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
            $answer['message'] = 'Prepare failed';
            $answer['error'] = $dbconn->errorInfo();
            return $answer;
        }

//    if (!$query->bind_param("s", $link)) {
////        echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
//        $answer['message'] = 'Binding parameters failed';
//        $answer['error'] = $query->errorInfo();
//        return $answer;
//    }

        if (!$query->execute([$link])) {
//        echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $answer['message'] = 'Execute failed';
            $answer['error'] = $query->errorInfo();
            return $answer;
        }

        $res = $query->fetchAll(PDO::FETCH_ASSOC);
//    if (!($res = $query->get_result())) {
////        echo "Getting result set failed: (" . $stmt->errno . ") " . $stmt->error;
//        $answer['message'] = 'Getting result set failed';
//        $answer['error'] = $query->errorInfo();
//        return $answer;
//    }

        if (count($res) == 0) {
            $answer['message'] = 'Link is not inserted correctly';
            return $answer;
        }
//    $res->data_seek(0);

        $answer['status'] = 1;
        $answer['link_id'] = $res[0]['link_id'];
        return $answer;
    }

    // status
    // 1 = link updated
    // 2 = link does not exist
    // link => LINKS
    public static function update($link_id) {
        $arr = json_decode(file_get_contents('php://input'), true);
        $answer = getAnswerPrototype();
        if (!isset($arr['status'])) {
            $answer['message'] = 'You must enter status';
            return $answer;
        }

        global $dbconn;
        if (!($query = $dbconn->prepare("UPDATE links SET status=?, update_time=? WHERE link_id=?"))) {
//        echo "Prepare failed: (" . $mysqli->errno . ") " . $mysqli->error;
            $answer['message'] = 'Prepare failed';
            $answer['error'] = $dbconn->errorInfo();
            return $answer;
        }

//    if (!$query->bind_param("ss", $link,date('Y/m/d G:i:s'))) {
////        echo "Binding parameters failed: (" . $stmt->errno . ") " . $stmt->error;
//        $answer['message'] = 'Binding parameters failed';
//        $answer['error'] = $query->errorInfo();
//        return $answer;
//    }

        if (!$query->execute([$arr['status'], date('Y/m/d G:i:s'), $link_id])) {
//        echo "Execute failed: (" . $stmt->errno . ") " . $stmt->error;
            $answer['message'] = 'Execute failed';
            $answer['error'] = $query->errorInfo();
            return $answer;
        }

        $res = getApi()->invoke(static::$base_route . $link_id, EpiRoute::httpGet);
        if ($res['status'] != 1) {
            return $res;
        }

        $answer['status'] = 1;
        $answer['message'] = 'Link is updated';
        $answer['link'] = $res['link'];
        return $answer;
    }
}
