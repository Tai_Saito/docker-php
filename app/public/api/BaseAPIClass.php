<?php

require_once('require.php');

class BaseAPIClass
{
    public static $class_name;
    public static $base_route;
    public static $http_methods = [
    ];

    public static function getRequestArray($methodName) {
        global $HTTP_METHODS;
        $method = static::$http_methods[$methodName];
        $result = $HTTP_METHODS[$method][1];
        return $result();
    }

    public static function apiInvoke($methodName, $arr) {
        global $HTTP_METHODS;
        $method = static::$http_methods[$methodName];
        $route = static::$base_route . $methodName;
        $methodArr = array($HTTP_METHODS[$method][0] => $arr);
        return getApi()->invoke($route, $method, $methodArr);
    }
}
