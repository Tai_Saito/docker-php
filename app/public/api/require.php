<?php

$HTTP_METHODS = [
    EpiRoute::httpGet => ['_GET', function() {
        $count = 1;
        if (isset($_GET['__route__'])) $count++;
        if (count($_GET) >= $count) return $_GET;
        return json_decode(file_get_contents('php://input'), true);
    }],
    EpiRoute::httpPost => ['_POST', function() {
        $count = 1;
        if (isset($_POST['__route__'])) $count++;
        if (count($_POST) >= $count) return $_POST;
        return json_decode(file_get_contents('php://input'), true);
    }],
    EpiRoute::httpPut => ['_PUT', function() {
    return json_decode(file_get_contents('php://input'), true);
}]
];

function getAnswerPrototype() {
    return [
        'status' => 5,
        'message' => ''
    ];
}

function send($msg)
{
    $json = (string)json_encode($msg);
    include('SimpleSender.php');
    $sender = new \brokers\SimpleSender();
    $sender->execute($json);
}
