import './links.css';
import React from 'react';
import Header from "../../components/header";

export default class Links extends React.Component {
    state = {
        loading: true,
        data: null
    };

    async componentDidMount() {
        const url = "http://" + window.location.host + "/api/links";
        const response = await fetch(url, {
            method: "GET",
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json"
            }
        }).catch(error => console.log('Error fetch-request: ',error.message));
        try {
            const data = await response.json();
            console.log(data.links);
            if (data.links) {
                this.setState({loading: false, data: data.links});
            } else {
                this.setState({loading: false});
            }
        } catch (error) {
            console.log(error);
        }
    }

    render() {
        return (
            <div>
                <Header/>
                <div className="main-info pt-4">
                    <h1 className="mb-3 flex-info">Links</h1>
                    {this.state.loading || !this.state.data ? (
                        <div>
                            <div>Loading...</div>
                        </div>
                    ) : (
                        <ul className="list-group mb-3">
                            {this.state.data.map(item => (
                                <li className="list-group-item list-group-item-dark"
                                    key={item.link_id}>
                                    <p className="h4">{item.status}</p>
                                    <a className="lead" href={item.link}>{item.status}</a>
                                </li>
                            ))}
                        </ul>
                    )}
                </div>
            </div>
        );
    }
}