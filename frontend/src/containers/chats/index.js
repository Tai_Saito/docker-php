import './chats.css';
import React from 'react';
import Header from "../../components/header";

export default class Chats extends React.Component {
	state = {
		loading: true,
		data: null
	};

	async componentDidMount() {
		const url = "http://" + window.location.host + "/api/chats";
		try {
			const response = await fetch(url, {
				method:  "GET",
				headers: {
					"Accept": "application/json",
					"Content-Type": "application/json"
				}
			}).catch(error => {
					console.log('Error fetch-request: ',error.message);
					this.setState({loading: false});
				});

			try {
				const data = await response.json();
				if (data.chats) {
					this.setState({data: data.chats});
				} else this.setState({loading: false});
			} catch (err) {
				console.log(error);
			}
		} catch (error) {
			console.log(error);
		}
		this.setState({loading: false});
	}

	render() {
		return (
			<div>
				<Header/>
				<div className="main-info pt-4">
					<h1 className="mb-3 flex-info">Existing chats</h1>
					{this.state.loading || !this.state.data ? (
						<div>
							<div>Loading...</div>
						</div>
						) : (
							<ul className="mb-3">
								{this.state.data.map(item => (
									<li className="list-group-item-dark"
										key={item.chat_id}>
										<p className="h4">Chat id: {item.theme_id}</p>
										<p class="lead">Age min: {item.age_min} age</p>
										<p class="lead">Age max: {item.age_max} age</p>
									</li>
								))}
							</ul>
					)}
				</div>
			</div>
		);
	}
}